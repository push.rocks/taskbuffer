import { tap, expect } from '@pushrocks/tapbundle';

import * as taskbuffer from '../ts/index.js';

let counter1 = 0;
let counter2 = 0;
let counter3 = 0;

tap.test('should run buffered', async (tools) => {
  const task = new taskbuffer.Task({
    name: 'a buffered task',
    taskFunction: async () => {
      counter1++;
      await tools.delayFor(2000);
      console.log(`task 1 ran ${counter1} times`);
    },
    buffered: true,
    bufferMax: 1,
    afterTask: () => {
      return task2;
    },
  });
  const task2 = new taskbuffer.Task({
    name: 'a buffered task',
    taskFunction: async () => {
      counter2++;
      await tools.delayFor(2000);
      console.log(`task2 ran ${counter2} times`);
    },
    buffered: true,
    bufferMax: 1,
    afterTask: () => {
      return task3;
    },
  });
  const task3 = new taskbuffer.Task({
    name: 'a buffered task',
    taskFunction: async () => {
      counter3++;
      await tools.delayFor(2000);
      console.log(`task3 ran ${counter3} times`);
    },
    buffered: true,
    bufferMax: 1,
  });
  while (counter1 < 10) {
    await tools.delayFor(5000);
    task.trigger();
  }
});

tap.start();
