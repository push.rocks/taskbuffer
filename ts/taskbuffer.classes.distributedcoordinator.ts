import { Task } from './taskbuffer.classes.task.js';
import * as plugins from './taskbuffer.plugins.js';

/**
 * constains all data for the final coordinator to actually make an informed decision
 */
export interface IDistributedTaskRequest {
  /**
   * this needs to correlate to the consultationResult
   */
  submitterRandomId: string;
  taskName: string;
  taskVersion: string;
  taskExecutionTime: number;
  taskExecutionTimeout: number;
  taskExecutionParallel: number;
  status: 'requesting' | 'gotRejected' | 'failed' | 'succeeded';
}

export interface IDistributedTaskRequestResult {
  /**
   * this needs to correlate to the decisionInfoBasis
   */
  submitterRandomId: string;
  considered: boolean;
  rank: string;
  reason: string;
  shouldTrigger: boolean;
}

export abstract class AbstractDistributedCoordinator {
  public abstract fireDistributedTaskRequest(infoBasisArg: IDistributedTaskRequest): Promise<IDistributedTaskRequestResult>
  public abstract updateDistributedTaskRequest(infoBasisArg: IDistributedTaskRequest): Promise<void>
}